package be.bluebilt.boot.launcher.bug;

import java.net.URLClassLoader;
import java.util.stream.Stream;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BootLauncherBugApplication {

	public static void main(String[] args) {
        printClassPath();		        
		SpringApplication.run(BootLauncherBugApplication.class, args);
	}

	private static void printClassPath() {
		URLClassLoader cl = (URLClassLoader)ClassLoader.getSystemClassLoader();
		Stream.of(cl.getURLs()).forEach(System.out::println);		
	}
}
